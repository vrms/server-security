## secure ssh quickstart

1. create new user and add it to the sudo/wheel/admin group
1. create ssh keypair on client `ssh-keygen -t rsa`
1. copy the public key to the server  
on the client run `ssh-copy-id [username]@[server_ip]`
1. edit **/etc/ssh/sshd_config**
 1. set `PermitRootLogin no`
 1. `PasswordAuthentication no`
 1. `AllowUser [username]`  
 (not 100% certain #3 does anything good though)
1. restart the ssh daemon for these changes to take effect with: `systemctl restart sshd`
