# ufw quickstart

source https://youtu.be/-CzvPjZ9hp8

---

### basic commands

`systemctl status/stop/start ufw`   manages the `ufw` service in general

`ufw disable`   deactivates ufw

`ufw enable`	activates ufw

`ufw reset`     removes all existing rules (and backs them up)

`ufw default deny incomming`    denies all incoming traffic

`ufw default allow outgoing`    allows all outgorin traffic

`ufw allow [protocol]/[port_nr.]`   allows incoming access based on protocol or port

`ufw deny [protocol]/[port_nr.]`    denies incoming access based on protocol or port

`sudo ufw deny from [15.15.15.51]`    denies all incoming access from the specified IP

`ufw status`    shows `active` either `inactive`

> beware: **specified rules are only listed in `active` state**  
> question: **how can you check the specifed rules when ufw state is `inactive`?**

`ufw status numbered`   shows rules with a number (which you will address for deleting a rule)

`ufw delete [rule_nr.]` removes the rule with the specific number. This will reorder to sequence of rules.

> good to know: you can use `--dry-run` in order to see what an ufw command **would** do

the ufw configuration file is: **/etc/default/ufw**


---

### simple initial config

1. `ufw disable`
2. `ufw reset`
3. `ufw default deny incoming`
4. `ufw default allow outgoing`
5. `ufw allow ssh`
6. `ufw allow http`
7. `ufw allow https`
8. `ufw enable`         be sure none of the rules set above will admin access to your server before enabling ufw

---

### notable

**Both** the ufw daemon (`systemctl start ufw`) **AND** ufw needs to be enabled (`ufw enable`) for it to apply the rules set.
